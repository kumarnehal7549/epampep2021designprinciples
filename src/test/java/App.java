import static org.junit.Assert.*;

import org.junit.Test;

public class App 
{
	Main o = new Main();

	@Test
	public void testAdd()
	{
		
		assertEquals(8,o.add(5, 3));
		assertEquals(-6,o.add(3, -9));
		assertEquals(-14,o.add(-5, -9));
	}
	
	@Test
	public void testSub() 
	{
		assertEquals(-2,o.sub(5, 7));
		assertEquals(-10,o.sub(-3, 7));
		assertEquals(1,o.sub(5, 4));
	}
	
	@Test
	public void testMul() 
	{
		assertEquals(35,o.mul(5, 7));
		assertEquals(-21,o.mul(-3, 7));
		assertEquals(20,o.mul(-5, -4));
	}
	
	@Test
	public void testDiv() 
	{
		assertEquals(3,o.div(15, 5));
		assertEquals(5,o.div(10, 2));
		assertEquals(5,o.div(15, 3));
		assertEquals(10,o.div(90, 9));
	}
}